﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleStore.BackEnd.DAL.Interfaces;
using SimpleStore.BackEnd.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace SimpleStore.BackEnd.RESTful.Controllers
{
    public class ProductController : ApiControllerGeneric<Product>
    {
        public ProductController(IProductRepository repository, ILogger<Product> log) : base(repository, log)
        {
        }

        [HttpGet("category/{categoryId}")]
        public  async Task<ActionResult> GetByCategoryId(int categoryId)
        {
            try
            {
                Log.LogInformation($"Получение продуктов по категории{categoryId}");
                var products = await ((IProductRepository)Repository).GetByCategoryId(categoryId);

                return Ok(products);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict();
            }
        }
    }
}
