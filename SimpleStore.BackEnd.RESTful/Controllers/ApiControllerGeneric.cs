﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleStore.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace SimpleStore.BackEnd.RESTful.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ApiControllerGeneric<TModel> : Controller where TModel : class
    {
        public readonly IRepository<TModel> Repository;
        public readonly ILogger<TModel> Log;

        public ApiControllerGeneric(IRepository<TModel> repository, ILogger<TModel> log)
        {
            Log = log;
            Repository = repository;
        }

        [HttpGet]
        public virtual async Task<ActionResult> Get()
        {
            try
            {
                Log.LogInformation("Получение данных " + typeof(TModel));
                var list = await Repository.GetAll();
                return Ok(list);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict();
            }
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult> Get(int id)
        {
            try
            {
                var model = await Repository.Get(id);
                if (model == null)
                {
                    return NoContent();
                }
                return Ok(model);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict();
            }
        }

        [HttpPost]
        public virtual async Task<ActionResult> Post([FromBody]TModel model)
        {
            try
            {
                if (model == null || !ModelState.IsValid)
                {
                    Log.LogError($"Не удалось добавить данные {typeof(TModel)}: {model}");
                    return BadRequest("Invalid State");
                }

                Log.LogInformation($"Добавление данных {typeof(TModel)}: {model}");

                await Repository.Add(model);

                await Repository.Save();

                return Ok(model);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict("Error while creating");
            }
        }

        [HttpPut("{id}")]
        public virtual async Task<ActionResult> Put(int id, [FromBody]TModel model)
        {
            try
            {
                if (model == null || !ModelState.IsValid)
                {
                    Log.LogError($"Не удалось изменить данные {typeof(TModel)}: {model}");
                    return BadRequest("Invalid State");
                }

                Log.LogInformation($"Редактирование данных {typeof(TModel)}: {model}");

                Repository.Update(model);

                await Repository.Save();

                return Ok(model);
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict("Error while updating");
            }
        }

        [HttpDelete("{id}")]
        public virtual async Task<ActionResult> Delete(int id)
        {
            try
            {
                var model = await Repository.Get(id);
                if (model == null)
                {
                    return BadRequest($"Don't find {typeof(TModel)} by id : {id}");
                }

                Log.LogInformation($"DELETE: {typeof(TModel)}" + id);

                Repository.Remove(model);

                await Repository.Save();
                
                return Ok();
            }
            catch (Exception exception)
            {
                Log.LogError(exception.Message);
                return Conflict("Error while deleting");
            }
        }
    }
}
