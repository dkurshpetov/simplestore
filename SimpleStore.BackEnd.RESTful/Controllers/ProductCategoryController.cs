﻿using Microsoft.Extensions.Logging;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Interfaces;

namespace SimpleStore.BackEnd.RESTful.Controllers
{
    public class ProductCategoryController : ApiControllerGeneric<ProductCategory>
    {
        public ProductCategoryController(IProductCategoryRepository repository, ILogger<ProductCategory> log) : base(repository, log)
        {
        }
    }
}
