﻿using Microsoft.Extensions.Logging;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Interfaces;

namespace SimpleStore.BackEnd.RESTful.Controllers
{
    public class OrderController : ApiControllerGeneric<Order>
    {
        public OrderController(IOrderRepository repository, ILogger<Order> log) : base(repository, log)
        {
        }
    }
}