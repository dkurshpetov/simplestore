﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SimpleStore.BackEnd.Domain.Entities
{
    public class EntityBase : AuditableEntity
    {
        [MaxLength(25)]
        [Required]
        public string Name { get; set; }

        [MaxLength(256)]
        [Required]
        public string Description { get; set; } 
    }
}
