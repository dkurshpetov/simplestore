﻿using System.Collections.Generic;

namespace SimpleStore.BackEnd.Domain.Entities
{
    public class ProductCategory : EntityBase
    {
        public ICollection<Product> Products { get; set; }
    }
}
