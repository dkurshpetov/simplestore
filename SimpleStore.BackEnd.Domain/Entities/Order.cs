﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleStore.BackEnd.Domain.Entities
{
    public class Order : EntityBase
    {
        public bool IsCheck { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Total { get; set; }
        public int TotalCount { get; set; }
        public ICollection<OrderItem> Items { get; set; }
    }
}