﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleStore.BackEnd.Domain.Entities
{
    public class OrderItem : EntityBase
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal UnitPrice { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal OldUnitPrice { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Total { get; set; }
    }
}