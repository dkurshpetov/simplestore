﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleStore.BackEnd.Domain.Entities
{
    public class AuditableEntity : IAuditableEntity
    {
        public int Id { get; set; }

        [MaxLength(256)]
        [JsonIgnore]
        public string CreatedBy { get; set; }

        [MaxLength(256)]
        [JsonIgnore]
        public string UpdatedBy { get; set; }

        [JsonIgnore]
        public DateTime UpdatedDate { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
    }
}


