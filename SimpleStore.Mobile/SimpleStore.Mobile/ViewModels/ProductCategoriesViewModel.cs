﻿using System.Threading.Tasks;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using Xamarin.Forms;

namespace SimpleStore.Mobile.ViewModels
{
    public class ProductCategoriesViewModel : ViewListBase<ProductCategory>
    {
        public Command AddCategoryCommand { get; set; }
        public Command ShowAddCategoryPageCommand { get; set; }
        
        public ProductCategoriesViewModel(INavigationService navigationService, IProductCategoryService productCategoryService):base(navigationService, productCategoryService)
        {
            Title = "Categories";

            ShowAddCategoryPageCommand = new Command(async () => await ShowAddCategoryPage());
            AddCategoryCommand = new Command<ProductCategory>(async (ProductCategory productCategory) => await AddCategory(productCategory));

            MessagingCenter.Subscribe<CreateCategoryViewModel, ProductCategory>(this, "SaveCategory", async (obj, category) =>
            {
                var productCategory = category as ProductCategory;
                await AddCategory(productCategory);
            });
        }

        protected override async Task OnListingSelected(ProductCategory category)
        {
            await base.OnListingSelected(category);
            await NavigationService.NavigateToAsync<ProductsViewModel>(category);
        }

        public async Task AddCategory(ProductCategory category)
        {
            var result = await Service.AddItemAsync(category);
            if (result != null)
            {
                Items.Add(result);
            }
        }

        public async Task ShowAddCategoryPage()
        {
            await NavigationService.NavigateToAsync<CreateCategoryViewModel>();
        }
    }
}
