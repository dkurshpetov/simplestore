﻿using System.Linq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SimpleStore.Mobile.ViewModels
{
    public class BasketViewModel : ViewListBase<BasketItem>
    {
        private readonly IOrderService _orderService;
        private int _badgeCount =0;
        private decimal _total = 0;
        

        public int BadgeCount
        {
            get { return _badgeCount; }
            set {
                SetProperty(ref _badgeCount, value);
            }
        }

        public decimal Total
        {
            get { return _total; }
            set {
                SetProperty(ref _total, value);
            }
        }

        

        public ICommand AddCommand => new Command<BasketItem>(async (item) => await AddItemAsync(item));
        public ICommand CheckoutCommand => new Command(async () => await CheckoutAsync());


        public BasketViewModel(INavigationService navigationService, IBasketService basketService, IOrderService orderService) : base(navigationService, basketService)
        {
            _orderService = orderService;
            Title = "Basket";


            MessagingCenter.Subscribe<ProductsViewModel, Product>(this, "AddProductToBasket", async (obj, product) =>
            {
                BadgeCount++;
   
                await AddProductAsync(product);
            });
        }


        protected override async Task ExecuteLoadItemsCommand()
        {
            await base.ExecuteLoadItemsCommand();
            BadgeCount=Items.Sum(x=>x.Quantity);
            await ReCalculateTotalAsync();
        }

        public async Task AddProductAsync(Product item)
        {

            var product = Items.FirstOrDefault(x => x.ProductId == item.Id);

            BadgeCount++;

            if (product != null)
            {
                product.Quantity++;
            }
            else
            {
                Items.Add(new BasketItem
                {
                    ProductId = item.Id,
                    ProductName = item.Name,
                    PictureUrl = item.ImageUrl,
                    UnitPrice = item.Price,
                    Quantity = 1
                });
            }

            

            await ReCalculateTotalAsync();

        }

        private async Task AddItemAsync(BasketItem item)
        {
            BadgeCount++;
            Items.Add(item);
            await ReCalculateTotalAsync();

        }

        private async Task ReCalculateTotalAsync()
        {
            Total = 0;

            if (Items == null)
            {
                return;
            }

            foreach (var orderItem in Items)
            {
                Total += (orderItem.Quantity * orderItem.UnitPrice);
            }

        }


        private async Task CheckoutAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (Items.Any())
            {
                await ReCalculateTotalAsync();
                var order = new Order
                {
                    Name = "Заказ",
                    Description = "This is order",
                    IsCheck = true,
                    Items = new List<OrderItem>(),
                    Total = Total,
                    TotalCount = BadgeCount
                };

                foreach (var item in Items)
                {
                    var orderItem = new OrderItem
                    {
                        Name = item.Name,
                        Description = item.Name,
                        Product = new Product {Id=item.ProductId},
                        UnitPrice= item.UnitPrice,
                        Quantity = item.Quantity,
                        Total = item.Total
                    };
                }

                try
                {
                    await _orderService.AddItemAsync(order);
                    Items.Clear();
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception);
                }
            }
            IsBusy = false;
        }
    }
}
