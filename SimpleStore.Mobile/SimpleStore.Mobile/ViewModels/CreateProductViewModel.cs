﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.Validations;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SimpleStore.Mobile.ViewModels
{
    public class CreateProductViewModel : ViewModelBase
    {
        private ValidatableObject<string> _name;
        private ValidatableObject<string> _description;
        private ValidatableObject<decimal> _price;
        private bool _isValid;


        public ValidatableObject<string> Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public ValidatableObject<string> Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        public ValidatableObject<decimal> Price
        {
            get { return _price; }
            set { SetProperty(ref _price, value); }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set { SetProperty(ref _isValid, value); }
        }

        public ICommand SaveProductCommand => new Command(async () => await SaveProduct());
        public ICommand CancelCommand => new Command(async () => await Cancel());
        public ICommand ValidateNameCommand => new Command(() => ValidateName());
        public ICommand ValidateDescriptionCommand => new Command(() => ValidateDescription());
        public ICommand ValidatePriceCommand => new Command(() => ValidatePrice());
        
        public CreateProductViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Add Product";
            _name = new ValidatableObject<string>();
            _description = new ValidatableObject<string>();
            _price = new ValidatableObject<decimal>();
            AddValidations();
        }

        private void AddValidations()
        {
            _name.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A name is required." });
            _description.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A description is required." });
            _price.Validations.Add(new IsNotRangeRule<decimal>{ ValidationMessage = "the price should be in the range of 0 to 10000", Min = 0, Max = 10000});
        }

        public bool Validate()
        {
            bool isValidName = ValidateName();
            bool isValidDescription = ValidateDescription();
            bool isPrice = ValidatePrice();
            return isValidName && isValidDescription && isPrice ;
        }

        private bool ValidateName()
        {
            return _name.Validate();
        }

        private bool ValidateDescription()
        {
            return _description.Validate();
        }

        private bool ValidatePrice()
        {
            return _price.Validate();
        }

        private async Task SaveProduct()
        {
            IsBusy = true;
            IsValid = true;
            bool isValid = Validate();

            if (isValid)
            {
                var category = new Product { Name = _name.Value, Description = _description.Value, Price = _price.Value};
                MessagingCenter.Send(this, "SaveProduct", category);
                await NavigationService.NavigateToBack();
            }

            IsBusy = false;
        }

        private async Task Cancel()
        {
            await NavigationService.NavigateToBack();
        }
    }
}