﻿using System;
using System.Windows.Input;
using SimpleStore.Mobile.Services;
using Xamarin.Forms;

namespace SimpleStore.Mobile.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        public AboutViewModel(INavigationService navigationService): base(navigationService)
        {
            Title = "About";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://xamarin.com/platform")));
        }

        public ICommand OpenWebCommand { get; }
    }
}