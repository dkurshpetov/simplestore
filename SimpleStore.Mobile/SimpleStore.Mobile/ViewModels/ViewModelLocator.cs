﻿
using SimpleStore.Mobile.RequestProvider;
using SimpleStore.Mobile.Services;
using System;
using System.Globalization;
using System.Reflection;
using TinyIoC;
using Xamarin.Forms;

namespace SimpleStore.Mobile.ViewModels
{
    public static class ViewModelLocator
    {
        private static TinyIoCContainer _container;

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);

        internal static void RegisterSingleton<T>(T navigation)
        {
            throw new NotImplementedException();
        }

        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(ViewModelLocator.AutoWireViewModelProperty);
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(ViewModelLocator.AutoWireViewModelProperty, value);
        }

        public static bool UseMockService { get; set; }

        static ViewModelLocator()
        {
            _container = new TinyIoCContainer();

            _container.Register<AboutViewModel>();
            _container.Register<ProductCategoriesViewModel>();
            _container.Register<ProductsViewModel>();
            _container.Register<BasketViewModel>();

            _container.Register<CreateCategoryViewModel>();
            _container.Register<CreateProductViewModel>();

            _container.Register<IRequestProvider, RequestProvider.RequestProvider>();
            _container.Register<INavigationService, NavigationService>();
        }

        public static void UpdateDependencies(bool useMockServices)
        {
            
            if (useMockServices)
            {

                _container.Register<IBasketService, BasketMockService>();
                _container.Register<IProductCategoryService, ProductCategoryMockService>();
                _container.Register<IProductService, ProductMockService>();
                _container.Register<IOrderService, OrderMockService>();
                UseMockService = true;
            }
            else
            {
                _container.Register<IBasketService, BasketMockService>();
                _container.Register<IProductCategoryService, ProductCategoryService>();
                _container.Register<IProductService, ProductService>();
                _container.Register<IOrderService, OrderService>();
                UseMockService = false;
            }
        }

        public static void RegisterSingleton<TInterface, T>() where TInterface : class where T : class, TInterface
        {
            _container.Register<TInterface, T>().AsSingleton();
        }

        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }
            var viewModel = _container.Resolve(viewModelType);
            view.BindingContext = viewModel;
        }
    }
}
