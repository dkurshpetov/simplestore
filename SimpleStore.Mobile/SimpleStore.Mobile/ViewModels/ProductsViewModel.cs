﻿using SimpleStore.Mobile.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using SimpleStore.Mobile.Services;
using Xamarin.Forms;
using System.Windows.Input;

namespace SimpleStore.Mobile.ViewModels
{
    public class ProductsViewModel : ViewListBase<Product>
    {
        private ProductCategory _category;

        public ICommand AddProductToBasketCommand => new Command(async () => await AddProductToBasket());
        public ICommand ShowAddProductCommand => new Command(async () => await ShowAddProduct());

        public ProductsViewModel(INavigationService navigationService, IProductService productService):base(navigationService, productService)
        {
            MessagingCenter.Subscribe<CreateProductViewModel, Product>(this, "SaveProduct", async (obj, item) =>
            {
                var newItem = item as Product;
                await AddProduct(newItem);
            });
        }

        public ProductCategory CurrentCategory
        {
            get { return _category; }
            set
            {
                Title = value.Name;
                SetProperty(ref _category, value);
            }
        }

        protected override async Task OnListingSelected(Product product)
        {
            await base.OnListingSelected(product);
            ItemSelected = product;
        }

        private async Task AddProductToBasket()
        {
            if (ItemSelected!=null)
            {
               MessagingCenter.Send(this, "AddProductToBasket", ItemSelected);
            }
            await Task.Delay(10);
        }

        protected override async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await ((IProductService)Service).GetItemsAsync(_category, true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task ShowAddProduct()
        {
            await NavigationService.NavigateToAsync<CreateProductViewModel>();
        }


        private async Task AddProduct(Product product)
        {
            product.CategoryId = CurrentCategory.Id;
            try
            {
                var result = await Service.AddItemAsync(product);
                if (result != null)
                {
                    Items.Add(result);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);        
            } 
        }
    }
}