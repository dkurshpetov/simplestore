﻿using SimpleStore.Mobile.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using SimpleStore.Mobile.Models;
using Xamarin.Forms;
using SimpleStore.Mobile.Validations;

namespace SimpleStore.Mobile.ViewModels
{
    public class CreateCategoryViewModel : ViewModelBase
    {
        private ValidatableObject<string> _name;
        private ValidatableObject<string> _description;
        private bool _isValid;


        public ValidatableObject<string> Name
        {
            get { return _name;}
            set { SetProperty(ref _name, value); }
        }

        public ValidatableObject<string> Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }


        public bool IsValid
        {
            get { return _isValid; }
            set { SetProperty(ref _isValid, value); }
        }


        public ICommand SaveCategoryCommand => new Command(async () => await SaveCategory());
        public ICommand CancelCommand => new Command(async () => await Cancel());
        public ICommand ValidateNameCommand => new Command(() => ValidateName());
        public ICommand ValidateDescriptionCommand => new Command(() => ValidateDescription());


        public CreateCategoryViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Add Category";
            _name = new ValidatableObject<string>();
            _description = new ValidatableObject<string>();
            AddValidations();
        }

        private void AddValidations()
        {
            _name.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A name is required." });
            _description.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A description is required." });
        }

        public bool Validate()
        {
            bool isValidName = ValidateName();
            bool isValidDescription = ValidateDescription();            
            return isValidName && isValidDescription;
        }

        private bool ValidateName()
        {
            return _name.Validate();
        }

        private bool ValidateDescription()
        {
            return _description.Validate();
        }

        private async Task SaveCategory()
        {
            IsBusy = true;
            IsValid = true;
            bool isValid = Validate();

            if (isValid)
            {
                var category = new ProductCategory {Name = _name.Value, Description = _description.Value};
                MessagingCenter.Send(this, "SaveCategory", category);
                await NavigationService.NavigateToBack();
            }

            IsBusy = false;
        }

        private async Task Cancel()
        {
            await NavigationService.NavigateToBack();
        }

    }
}
