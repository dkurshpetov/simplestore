﻿using SimpleStore.Mobile.Services;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SimpleStore.Mobile.ViewModels
{
    public class ViewListBase<T> : ViewModelBase where T:class
    {
        private ObservableCollection<T> _items;
        public Command LoadItemsCommand { get; set; }
        protected readonly IDataStore<T> Service;
        public Command ListingSelectedCommand { get; set; }
        public T ItemSelected { get; set; }

        public ObservableCollection<T> Items
        {
            get { return _items; }
            set
            {

                SetProperty(ref _items, value);
            }
        }

        
        public ViewListBase(INavigationService navigationService, IDataStore<T> service):base(navigationService)
        {
            Service = service;
            Items = new ObservableCollection<T>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            ListingSelectedCommand = new Command<T>(async (T item) => await OnListingSelected(item));
        }

        protected virtual async Task OnListingSelected(T item)
        {
            if (item == null)
                return;
            await Task.Delay(10);

            ItemSelected = null;
        }

        protected virtual async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await Service.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
