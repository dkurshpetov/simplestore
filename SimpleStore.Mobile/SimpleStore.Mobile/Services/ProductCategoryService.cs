﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;

namespace SimpleStore.Mobile.Services
{
    public class ProductCategoryService : DataStore<ProductCategory>, IProductCategoryService
    {   
        public ProductCategoryService(IRequestProvider requestProvider) : base(requestProvider)
        {
            ApiUrlBase = "/api/productCategory";
        }
    }
}
