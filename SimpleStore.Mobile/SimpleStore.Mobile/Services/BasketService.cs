﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;

namespace SimpleStore.Mobile.Services
{
    public class BasketService : DataStore<BasketItem>, IBasketService
    {
        public BasketService(IRequestProvider requestProvider) : base(requestProvider)
        {
            ApiUrlBase = "/api/order";
        }
    }
}
