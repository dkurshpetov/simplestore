﻿using SimpleStore.Mobile.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleStore.Mobile.Services
{
    public interface IProductService:IDataStore<Product>
    {
        Task<IEnumerable<Product>> GetItemsAsync(ProductCategory _category, bool forceRefresh = false);
    }
}