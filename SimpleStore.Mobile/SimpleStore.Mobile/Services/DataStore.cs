﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleStore.Mobile.Helpers;

namespace SimpleStore.Mobile.Services
{
    public abstract class DataStore<T> : IDataStore<T> where T : ModelBase<long>
    {
        protected readonly IRequestProvider RequestProvider;
        public string ApiUrlBase;
        
        public DataStore(IRequestProvider requestProvider)
        {
            RequestProvider = requestProvider;
        }

        public async Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false)
        {

            var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}");


            IEnumerable<T> list;
            try
            {
                list = await RequestProvider.GetAsync<IEnumerable<T>>(uri, GlobalSetting.Instance.Token);
            }
            catch (Exception)
            {
                list = null;
            }
            
            return list;
        }


        public async Task<T> GetItemAsync(long id)
        {

            var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}", $"{id}");


            T model;
            try
            {
                model = await RequestProvider.GetAsync<T>(uri, GlobalSetting.Instance.Token);
            }
            catch (Exception)
            {
                model = null;
            }

            return model;
        }

        public async Task<T> AddItemAsync(T item)
        {
            var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}");

            var result = await RequestProvider.PostAsync(uri, item, GlobalSetting.Instance.Token);
            return result;
        }

        public async Task<bool> UpdateItemAsync(T item)
        {
            try {
                var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}", $"{item.Id}" );

                var result = await RequestProvider.PostAsync(uri, item, GlobalSetting.Instance.Token);
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }

        public async Task<bool> DeleteItemAsync(long id)
        {
            try
            {
                var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}", $"{id}");

                await RequestProvider.DeleteAsync(uri, GlobalSetting.Instance.Token);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
