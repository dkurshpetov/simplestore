﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(ProductMockService))]
namespace SimpleStore.Mobile.Services
{
    public class ProductMockService : MockDataStore<Product>, IProductService
    {
        public async Task<IEnumerable<Product>> GetItemsAsync(ProductCategory _category, bool forceRefresh = false)
        {
            return await Task.FromResult(Items.Where(x=>x.CategoryId==_category.Id));
        }

        public override void InitData()
        {
            Items = new List<Product>
            {
                new Product { Id = 1, Name = "First product", ImageUrl ="noimage.png", Price = 15, Description="This is an item description.", CategoryId = 1}, 
                new Product { Id = 2, Name = "Second product", ImageUrl ="noimage.png", Price = 1, Description="This is an item description.", CategoryId = 1},
            };
        }
    }
}
