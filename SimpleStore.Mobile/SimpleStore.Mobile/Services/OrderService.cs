﻿using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;

namespace SimpleStore.Mobile.Services
{
    public class OrderService : DataStore<Order>, IOrderService
    {
        public OrderService(IRequestProvider requestProvider) : base(requestProvider)
        {
            ApiUrlBase = "/api/order";
        }
    }
}
