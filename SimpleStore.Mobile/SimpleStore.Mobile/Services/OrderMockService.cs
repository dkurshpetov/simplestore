﻿using SimpleStore.Mobile.Models;
using System.Collections.Generic;

namespace SimpleStore.Mobile.Services
{
    public class OrderMockService : MockDataStore<Order>, IOrderService
    {
        public override void InitData()
        {
            Items = new List<Order>();
        }
    }
}
