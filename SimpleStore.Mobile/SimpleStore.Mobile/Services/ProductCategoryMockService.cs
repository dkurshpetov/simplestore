﻿using System.Collections.Generic;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;

[assembly: Xamarin.Forms.Dependency(typeof(ProductCategoryMockService))]
namespace SimpleStore.Mobile.Services
{
    public class ProductCategoryMockService : MockDataStore<ProductCategory>, IProductCategoryService
    {
        public override void InitData()
        {
            Items = new List<ProductCategory>
            {
                new ProductCategory { Id = 1, Name = "First category", Description="This is an item description." },
                new ProductCategory { Id = 2, Name = "Second category", Description="This is an item description." },
                new ProductCategory { Id = 3, Name = "Third category", Description="This is an item description." },
                new ProductCategory { Id = 4, Name = "Fourth category", Description="This is an item description." },
                new ProductCategory { Id = 5, Name = "Fifth category", Description="This is an item description." },
                new ProductCategory { Id = 6, Name = "Sixth category", Description="This is an item description." },
            };
        }
    }
}

