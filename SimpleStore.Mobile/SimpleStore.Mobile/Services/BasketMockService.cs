﻿using System.Collections.Generic;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;

[assembly: Xamarin.Forms.Dependency(typeof(BasketMockService))]
namespace SimpleStore.Mobile.Services
{
    public class BasketMockService : MockDataStore<BasketItem>, IBasketService
    {
        public BasketMockService()
        {
        }

        public override void InitData()
        {
            Items = new List<BasketItem>();
        }
    }
}