﻿using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.ViewModels;
using System;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(NavigationService))]
namespace SimpleStore.Mobile.Services
{
    public class NavigationService : INavigationService
    {
        public NavigationService()
        {       
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(params object[] parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public async Task NavigateToBack() 
        {
            var navigationPage = Application.Current.MainPage.Navigation;
            if (navigationPage != null)
            {
                await navigationPage.PopModalAsync();
            }
        }


        private async Task InternalNavigateToAsync(Type viewModelType, params object[] parameter)
        {
            Page page = CreatePage(viewModelType, parameter);

            var navigationPage = Application.Current.MainPage.Navigation;
            if (navigationPage != null)
            {
                await navigationPage.PushModalAsync(new NavigationPage(page));
            }
        }

        private Page CreatePage(Type viewModelType, params object[] parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType, args: parameter) as Page;
            return page;
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("ViewModels", "Views").Replace("ViewModel", "Page");
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }
    }
}