﻿using SimpleStore.Mobile.ViewModels;
using System.Threading.Tasks;

namespace SimpleStore.Mobile.Services
{
    public interface INavigationService
    {
        Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase;
        Task NavigateToAsync<TViewModel>(params object[] parameter) where TViewModel : ViewModelBase;
        Task NavigateToBack();
    }
}
