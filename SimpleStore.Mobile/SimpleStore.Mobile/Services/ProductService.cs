﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleStore.Mobile.Helpers;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;

namespace SimpleStore.Mobile.Services
{
    public class ProductService : DataStore<Product>, IProductService
    {
        public ProductService(IRequestProvider requestProvider) : base(requestProvider)
        {
            ApiUrlBase = "/api/product";
        }

        public async Task<IEnumerable<Product>> GetItemsAsync(ProductCategory category, bool forceRefresh = false)
        {
            var uri = UriHelper.CombineUri(GlobalSetting.Instance.GatewayShoppingEndpoint, $"{ApiUrlBase}", "category", $"{category.Id}");

            IEnumerable<Product> list;
            try
            {
                list = await RequestProvider.GetAsync<IEnumerable<Product>>(uri, GlobalSetting.Instance.Token);
            }
            catch (Exception)
            {
                list = null;
            }
            return list;
        }
    }
}
