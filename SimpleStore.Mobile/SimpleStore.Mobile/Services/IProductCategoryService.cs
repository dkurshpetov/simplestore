﻿using SimpleStore.Mobile.Models;

namespace SimpleStore.Mobile.Services
{
    public interface IProductCategoryService : IDataStore<ProductCategory>
    {
    }
}
