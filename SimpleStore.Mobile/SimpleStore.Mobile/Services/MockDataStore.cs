﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleStore.Mobile.Models;

namespace SimpleStore.Mobile.Services
{
    public abstract class MockDataStore<T> : IDataStore<T> where T : ModelBase<long>
    {
        public List<T> Items;

        public MockDataStore()
        {
            Items = new List<T>();
            InitData();    
        }

        public abstract void InitData();

        public async Task<T> AddItemAsync(T item)
        {
            Items.Add(item);

            return await Task.FromResult(item);
        }

        public async Task<bool> UpdateItemAsync(T item)
        {
            var oldItem = Items.Where((T arg) => arg.Id == item.Id).FirstOrDefault();
            Items.Remove(oldItem);
            Items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(long id)
        {
            var oldItem = Items.Where((T arg) => arg.Id == id).FirstOrDefault();
            Items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<T> GetItemAsync(long id)
        {
            return await Task.FromResult(Items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(Items);
        }
    }
}