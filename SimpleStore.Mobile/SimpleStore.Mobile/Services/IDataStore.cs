﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleStore.Mobile.Services
{
    public interface IDataStore<T>
    {
        Task<T> AddItemAsync(T item);
        Task<bool> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(long id);
        Task<T> GetItemAsync(long id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
