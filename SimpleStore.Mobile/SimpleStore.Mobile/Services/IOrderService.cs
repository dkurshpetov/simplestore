﻿using SimpleStore.Mobile.Models;

namespace SimpleStore.Mobile.Services
{
    public interface IOrderService : IDataStore<Order>
    {
    }
}
