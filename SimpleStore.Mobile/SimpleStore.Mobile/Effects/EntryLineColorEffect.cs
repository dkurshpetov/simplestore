﻿using Xamarin.Forms;

namespace SimpleStore.Mobile.Effects
{
	public class EntryLineColorEffect : RoutingEffect
	{
		public EntryLineColorEffect() : base("SimpleStore.EntryLineColorEffect")
		{
		}
	}
}
