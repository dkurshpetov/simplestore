﻿namespace SimpleStore.Mobile
{
    public class GlobalSetting
    {
        public static GlobalSetting Instance { get; } = new GlobalSetting();
        public string GatewayShoppingEndpoint { get; set; } = "https://simplestoreweb.azurewebsites.net/";
        public string Token { get; set; }

        public GlobalSetting()
        {
            
        }
    }
}