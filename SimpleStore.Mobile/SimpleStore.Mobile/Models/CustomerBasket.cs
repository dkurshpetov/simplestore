﻿using System.Collections.Generic;

namespace SimpleStore.Mobile.Models
{
    public class CustomerBasket:ModelBase<long>
    {
        public string BuyerId { get; set; }
        public List<BasketItem> Items { get; set; }
    }
}