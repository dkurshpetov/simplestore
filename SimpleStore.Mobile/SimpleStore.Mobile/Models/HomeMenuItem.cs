﻿namespace SimpleStore.Mobile.Models
{
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }
        public string Title { get; set; }
    }
}
