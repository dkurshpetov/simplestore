﻿namespace SimpleStore.Mobile.Models
{
    public enum MenuItemType
    {        
        Categories,
        ShopingCart,
        About
    }
}
