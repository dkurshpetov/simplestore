﻿namespace SimpleStore.Mobile.Models
{
    public class BasketItem:ModelBase<long>
    {
        public long ProductId { get; set; }

        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal OldUnitPrice { get; set; }

        public bool HasNewPrice
        {
            get
            {
                return OldUnitPrice != 0.0m;
            }
        }

        public int Quantity { get; set; }
        
        public string PictureUrl { get; set; }

        public decimal Total { get { return Quantity * UnitPrice; } }

        public override string ToString()
        {
            return $"Product Id: {ProductId}, Quantity: {Quantity}";
        }
    }
}
