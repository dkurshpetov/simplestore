﻿namespace SimpleStore.Mobile.Models
{
    public class OrderItem:ModelBase<long>
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal OldUnitPrice { get; set; }
        public decimal Total { get; set; }
    }
}