﻿using System.Collections.Generic;

namespace SimpleStore.Mobile.Models
{
    public class Order : ModelBase<long>
    {
        public bool IsCheck { get; set; }
        public decimal Total { get; set; }
        public int TotalCount { get; set; }
        public ICollection<OrderItem> Items { get; set; }   
    }
}
