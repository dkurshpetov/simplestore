﻿namespace SimpleStore.Mobile.Models
{
    public class ModelBase<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return $"Id={Id} Name={Name} Description={Description}";
        }
    }
}
