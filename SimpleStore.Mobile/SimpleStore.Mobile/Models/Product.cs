﻿namespace SimpleStore.Mobile.Models
{
    public class Product : ModelBase<long>
    {
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public long CategoryId { get; set; }
    }
}