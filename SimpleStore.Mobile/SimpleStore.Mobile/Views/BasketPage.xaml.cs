﻿using SimpleStore.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasketPage : ContentPage
    {
        private BasketViewModel viewModel;

        public BasketPage()
        {
            InitializeComponent();

            BindingContext = viewModel = ViewModelLocator.Resolve<BasketViewModel>(); 

        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

    }


}
