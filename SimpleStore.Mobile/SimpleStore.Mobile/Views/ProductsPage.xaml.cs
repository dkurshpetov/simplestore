﻿using SimpleStore.Mobile.ViewModels;
using SimpleStore.Mobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsPage : ContentPage
    {
        private ProductsViewModel viewModel;

        public ProductsPage(ProductCategory category)
        {
            InitializeComponent();
            viewModel = ViewModelLocator.Resolve<ProductsViewModel>();
            viewModel.CurrentCategory = category;
            BindingContext = viewModel;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
