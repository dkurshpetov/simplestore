﻿using SimpleStore.Mobile.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductDetailPage : ContentPage
	{
        public Product Product { get; set; }

		public ProductDetailPage (Product product)
		{
			InitializeComponent ();
            Product = product;
            BindingContext = this;

        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}