﻿using SimpleStore.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateProductPage : ContentPage
    {
        private CreateProductViewModel _viewModel;
        public CreateProductPage()
        {

            InitializeComponent();
            BindingContext = _viewModel = ViewModelLocator.Resolve<CreateProductViewModel>();
        }
    }
}