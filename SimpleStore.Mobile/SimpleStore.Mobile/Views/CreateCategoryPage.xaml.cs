﻿using SimpleStore.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateCategoryPage : ContentPage
	{
        private CreateCategoryViewModel _viewModel;

        public CreateCategoryPage ()
		{
			InitializeComponent ();
            
            BindingContext = _viewModel = ViewModelLocator.Resolve<CreateCategoryViewModel>(); ;
        }

    }
}