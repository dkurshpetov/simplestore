﻿using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
          
            BindingContext = new AboutViewModel(new NavigationService());
        }

    }
}