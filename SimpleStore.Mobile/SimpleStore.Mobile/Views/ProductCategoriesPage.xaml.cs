﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SimpleStore.Mobile.ViewModels;

namespace SimpleStore.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductCategoriesPage :  ContentPage
    {
        private ProductCategoriesViewModel viewModel;

        public ProductCategoriesPage()
        {
            InitializeComponent();
            
            BindingContext = viewModel = ViewModelLocator.Resolve<ProductCategoriesViewModel>(); 
        }

        

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        
    }
}
