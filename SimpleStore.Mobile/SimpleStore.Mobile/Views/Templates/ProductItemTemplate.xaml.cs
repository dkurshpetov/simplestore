﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SimpleStore.Mobile.Views.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductItemTemplate : ContentView
	{
		public ProductItemTemplate()
		{
			InitializeComponent ();
		}
	}
}