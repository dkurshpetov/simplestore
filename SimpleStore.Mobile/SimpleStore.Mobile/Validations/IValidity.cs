﻿namespace SimpleStore.Mobile.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
