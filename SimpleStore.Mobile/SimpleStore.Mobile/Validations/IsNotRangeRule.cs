﻿using System;

namespace SimpleStore.Mobile.Validations
{
    public class IsNotRangeRule<T> : IValidationRule<T> where T : IComparable<T>
    {
        public string ValidationMessage { get; set; }
        public T Min { get; set; }
        public T Max { get; set; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }

            var resultMin = value.CompareTo(Min);

            if (resultMin <= 0)
            {
                return false;
            }
            else
            {
                var resultMax = value.CompareTo(Max);

                if (resultMax>0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}


