﻿using System;
using SimpleStore.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SimpleStore.Mobile.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SimpleStore.Mobile
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            ViewModelLocator.UpdateDependencies(false);

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
