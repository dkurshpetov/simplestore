﻿using System.Collections.Generic;
using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.ViewModels;
using Xunit;

namespace SimpleStore.Test.Mobile.ViewModels
{
    public class ProductsViewModelTest
    {
        private readonly ProductsViewModel _viewModel;
        private readonly Mock<IProductService> _service;
        private readonly Mock<INavigationService> _navigation;

        public ProductsViewModelTest()
        {
            _service = new Mock<IProductService>();
            _navigation = new Mock<INavigationService>();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Name = "Test Category",
                Description = "Category for test"
                
            };
            _viewModel = new ProductsViewModel(_navigation.Object, _service.Object)
            {
                CurrentCategory = productCategory
            };
        }

        [Fact]
        public void ShouldInitDefaultValue()
        {
            Assert.False(_viewModel.IsBusy);
            Assert.Empty(_viewModel.Items);
            Assert.Equal("Test Category", _viewModel.Title);
            Assert.NotNull(_viewModel.LoadItemsCommand);
            Assert.NotNull(_viewModel.CurrentCategory);
        }

        [Fact]
        public void IsBusyPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("IsBusy"))
                    invoked = true;
            };

            _viewModel.IsBusy = true;
            Assert.True(invoked);
        }

        [Fact]
        public void TitlePropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Title"))
                    invoked = true;
            };

            _viewModel.Title = "title";
            Assert.True(invoked);
        }

        [Fact]
        public void CurrentCategoryPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("CurrentCategory"))
                    invoked = true;
            };

            _viewModel.CurrentCategory = new ProductCategory();
            Assert.True(invoked);
        }

        [Fact]
        public void ShouldLoadData()
        {
            _service.Setup(x => x.GetItemsAsync(It.IsAny<ProductCategory>(), It.IsAny<bool>()))
                .ReturnsAsync(new List<Product>()
                {
                    new Product(),
                    new Product(),
                    new Product()
                });
              
            _viewModel.LoadItemsCommand.Execute(null);
            Assert.NotEmpty(_viewModel.Items);
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public void ShouldShowAddProduct()
        {
            _viewModel.ShowAddProductCommand.Execute(null);
            _navigation.Verify(n => n.NavigateToAsync<CreateProductViewModel>(), Times.AtMostOnce);
        }
    }
}
