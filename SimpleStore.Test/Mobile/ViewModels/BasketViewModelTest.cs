﻿using System.Collections.Generic;
using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.ViewModels;
using Xunit;

namespace SimpleStore.Test.Mobile.ViewModels
{
    public class BasketViewModelTest
    { 
        private readonly BasketViewModel _viewModel;
        private readonly Mock<IBasketService> _service;
        private readonly Mock<INavigationService> _navigation;
        private readonly Mock<IOrderService> _orderService;

        public BasketViewModelTest()
        {
            _service = new Mock<IBasketService>();
            _orderService = new Mock<IOrderService>();
            _navigation = new Mock<INavigationService>();
            _viewModel = new BasketViewModel(_navigation.Object, _service.Object, _orderService.Object);
        }

        [Fact]
        public void ShouldInitDefaultValue()
        {
            Assert.False(_viewModel.IsBusy);
            Assert.Empty(_viewModel.Items);
            Assert.Equal("Basket", _viewModel.Title);
            Assert.NotNull(_viewModel.LoadItemsCommand);
        }

        [Fact]
        public void IsBusyPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("IsBusy"))
                    invoked = true;
            };

            _viewModel.IsBusy = true;
            Assert.True(invoked);
        }


        [Fact]
        public void TitlePropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Title"))
                    invoked = true;
            };

            _viewModel.Title = "title";
            Assert.True(invoked);
        }

        [Fact]
        public void BadgeCountPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("BadgeCount"))
                    invoked = true;
            };

            _viewModel.BadgeCount = 5;
            Assert.True(invoked);
        }

        [Fact]
        public void TotalPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Total"))
                    invoked = true;
            };

            _viewModel.Total = 5;
            Assert.True(invoked);
        }

        [Fact]
        public void ShouldLoadData()
        {
            _service.Setup(x => x.GetItemsAsync(It.IsAny<bool>()))
                .ReturnsAsync(new List<BasketItem>
                {
                    new BasketItem(),
                    new BasketItem(),
                    new BasketItem(),
                    new BasketItem()
                });
                
            _viewModel.LoadItemsCommand.Execute(null);
            Assert.NotEmpty(_viewModel.Items);
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public void ShouldAddItem()
        {
            _viewModel.AddCommand.Execute(new BasketItem{Id = 1, Name = "", ProductId = 1, ProductName = "product", UnitPrice = 25.6M, Quantity = 6});

            Assert.NotEmpty(_viewModel.Items);

            Assert.Equal(153.6M,_viewModel.Total);

            Assert.Equal(1, _viewModel.BadgeCount);
            
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public async void ShouldAddProduct()
        {
            await _viewModel.AddProductAsync(new Product {Id = 1, Name = "product", Price = 120});
            await _viewModel.AddProductAsync(new Product { Id = 1, Name = "product", Price = 120 });
            await _viewModel.AddProductAsync(new Product { Id = 1, Name = "product", Price = 120 });

            await _viewModel.AddProductAsync(new Product { Id = 2, Name = "product", Price = 640 });

            Assert.NotEmpty(_viewModel.Items);

            Assert.Equal(1000, _viewModel.Total);

            Assert.Equal(4, _viewModel.BadgeCount);

            Assert.Equal(2, _viewModel.Items.Count);

            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public async void ShouldCheckoutProduct()
        {
            await _viewModel.AddProductAsync(new Product { Id = 1, Name = "product", Price = 120 });
            await _viewModel.AddProductAsync(new Product { Id = 1, Name = "product", Price = 120 });
            await _viewModel.AddProductAsync(new Product { Id = 1, Name = "product", Price = 120 });

            _viewModel.CheckoutCommand.Execute(null);

            _orderService.Verify(x => x.AddItemAsync(It.IsAny<Order>()));

            Assert.False(_viewModel.IsBusy);
        }
    }
}
