﻿using System.Collections.Generic;
using SimpleStore.Mobile.ViewModels;
using SimpleStore.Mobile.Services;
using Moq;
using SimpleStore.Mobile.Models;
using Xunit;

namespace SimpleStore.Test.Mobile.ViewModels
{
    public class ProductCategoriesViewModelTest
    {
        private ProductCategoriesViewModel _viewModel;
        private Mock<IProductCategoryService> _service; 
        private Mock<INavigationService> _navigation;

        public ProductCategoriesViewModelTest()
        {
            _service = new Mock<IProductCategoryService>();

            _navigation = new Mock<INavigationService>();
            _viewModel = new ProductCategoriesViewModel(_navigation.Object,_service.Object);
        }

        [Fact]
        public void ShouldInitDefaultValue()
        {
            Assert.False(_viewModel.IsBusy);
            Assert.Empty(_viewModel.Items);
            Assert.Equal("Categories", _viewModel.Title);
            Assert.NotNull(_viewModel.LoadItemsCommand);
        }

        [Fact]
        public void IsBusyPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("IsBusy"))
                    invoked = true;
            };

            _viewModel.IsBusy = true;
            Assert.True(invoked);
        }


        [Fact]
        public void TitlePropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Title"))
                    invoked = true;
            };

            _viewModel.Title = "title";
            Assert.True(invoked);
        }

        [Fact]
        public void ShouldLoadData()
        {
            _service.Setup(x => x.GetItemsAsync(It.IsAny<bool>())).ReturnsAsync(new List<ProductCategory>()
                {
                   new ProductCategory(),
                   new ProductCategory(),
                   new ProductCategory(),
                   new ProductCategory(),
                }
            );
            _viewModel.LoadItemsCommand.Execute(null);
            Assert.NotEmpty(_viewModel.Items);
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public void ShoulAddCategory()
        {
            _service.Setup(x => x.AddItemAsync(It.IsAny<ProductCategory>())).ReturnsAsync(new ProductCategory());
         
            _viewModel.Items.Clear();

            _viewModel.AddCategoryCommand.Execute(new ProductCategory());

            Assert.NotEmpty(_viewModel.Items);
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public void ShouldShowAddCategoryPage()
        {
            _viewModel.ShowAddCategoryPageCommand.Execute(null);
            _navigation.Verify(n=>n.NavigateToAsync<CreateProductViewModel>(), Times.AtMostOnce);
        }

        [Fact]
        public void ShouldShowProductsPage()
        {
            _viewModel.ListingSelectedCommand.Execute(new ProductCategory());
            _navigation.Verify(n => n.NavigateToAsync<ProductsViewModel>(It.IsAny<ProductCategory>()), Times.AtMostOnce);
        }
    }
}
