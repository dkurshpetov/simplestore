﻿using Moq;
using SimpleStore.Mobile.Services;
using SimpleStore.Mobile.ViewModels;
using Xunit;

namespace SimpleStore.Test.Mobile.ViewModels
{
    public class CreateProductTest
    {
        private readonly CreateProductViewModel _viewModel;
        private readonly Mock<INavigationService> _navigation;

        public CreateProductTest()
        {
            _navigation = new Mock<INavigationService>();
            _viewModel = new CreateProductViewModel(_navigation.Object);
        }

        [Fact]
        public void ShouldInitDefaultValue()
        {
            Assert.False(_viewModel.IsBusy);
            Assert.Equal("Add Product", _viewModel.Title);

            Assert.NotNull(_viewModel.ValidateDescriptionCommand);
            Assert.NotNull(_viewModel.ValidateNameCommand);

            Assert.False(_viewModel.IsValid);

            Assert.Null(_viewModel.Name.Value);
            Assert.NotEmpty(_viewModel.Name.Validations);
            Assert.True(_viewModel.Name.IsValid);

            Assert.Null(_viewModel.Description.Value);
            Assert.NotEmpty(_viewModel.Description.Validations);
            Assert.True(_viewModel.Description.IsValid);

            Assert.NotEmpty(_viewModel.Price.Validations);
            Assert.True(_viewModel.Price.IsValid);
        }

        [Fact]
        public void IsBusyPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("IsBusy"))
                    invoked = true;
            };

            _viewModel.IsBusy = true;
            Assert.True(invoked);
        }

        [Fact]
        public void TitlePropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("Title"))
                    invoked = true;
            };

            _viewModel.Title = "title";
            Assert.True(invoked);
        }

        [Fact]
        public void IsValidPropertyShouldRaisePropertyChanged()
        {
            var invoked = false;

            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("IsValid"))
                    invoked = true;
            };

            _viewModel.IsValid = true;
            Assert.True(invoked);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void NameShoulByNotValid(string value)
        {
            _viewModel.Name.Value = value;
            _viewModel.ValidateNameCommand.Execute(null);
            Assert.False(_viewModel.IsValid);
            Assert.False(_viewModel.Name.IsValid);
            Assert.NotEmpty(_viewModel.Name.Errors);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void DescreptionShouldByNotValid(string value)
        {
            _viewModel.Description.Value = value;
            _viewModel.ValidateDescriptionCommand.Execute(null);
            Assert.False(_viewModel.IsValid);
            Assert.False(_viewModel.Description.IsValid);
            Assert.NotEmpty(_viewModel.Description.Errors);
        }

        [Theory]
        [InlineData(-15)]
        [InlineData(150000)]
        public void PriceShouldByNotValid(decimal value)
        {
            _viewModel.Price.Value = value;
            _viewModel.ValidatePriceCommand.Execute(null);
            Assert.False(_viewModel.IsValid);
            Assert.False(_viewModel.Price.IsValid);
            Assert.NotEmpty(_viewModel.Price.Errors);
        }

        [Theory]
        [InlineData("test name")]
        public void NameShouldByValid(string value)
        {
            _viewModel.Name.Value = value;
            _viewModel.ValidateNameCommand.Execute(null);

            Assert.True(_viewModel.Name.IsValid);
            Assert.Empty(_viewModel.Name.Errors);
        }

        [Theory]
        [InlineData("test description")]
        public void DescreptionShouldByValid(string value)
        {
            _viewModel.Description.Value = value;
            _viewModel.ValidateDescriptionCommand.Execute(null);

            Assert.True(_viewModel.Description.IsValid);
            Assert.Empty(_viewModel.Description.Errors);
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("name", "")]
        [InlineData("", "descr")]
        public void IsValidShouldByFalse(string name, string description)
        {
            _viewModel.Name.Value = name;
            _viewModel.Description.Value = description;
            _viewModel.IsValid = _viewModel.Validate();
            Assert.False(_viewModel.IsValid);
        }

        [Theory]
        [InlineData("name", "descr", 15)]
        public void IsValidShouldByTrue(string name, string description, decimal price)
        {
            _viewModel.Name.Value = name;
            _viewModel.Description.Value = description;
            _viewModel.Price.Value = price;
            _viewModel.IsValid = _viewModel.Validate();
            Assert.True(_viewModel.IsValid);
        }

        [Fact]
        public void ShouldBySave()
        {
            _viewModel.Name.Value = "name";
            _viewModel.Description.Value = "description";
            _viewModel.Price.Value = 15.78m;

            _viewModel.SaveProductCommand.Execute(null);

            _navigation.Verify(n => n.NavigateToBack(), Times.AtLeastOnce);
            Assert.True(_viewModel.IsValid);
            Assert.False(_viewModel.IsBusy);
        }

        [Fact]
        public void ShouldByCancel()
        {
            _viewModel.CancelCommand.Execute(null);
            _navigation.Verify(n=>n.NavigateToBack(), Times.AtLeastOnce);
        }
    }
}
