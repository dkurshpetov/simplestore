﻿using SimpleStore.Mobile.Behaviors;

namespace SimpleStore.Test.Mobile.Behaviors
{
    internal class MockEventToCommandBehavior : EventToCommandBehavior
    {
        public void RaiseEvent(params object[] args)
        {
            _handler.DynamicInvoke(args);
        }
    }
}