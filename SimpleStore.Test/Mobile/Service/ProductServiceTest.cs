﻿
using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;
using SimpleStore.Mobile.Services;
using System.Collections.Generic;
using Xunit;

namespace SimpleStore.Test.Mobile.Service
{
    public class ProductServiceTest
    {
        public IProductService _categoryService { get; set; }
        public Mock<IRequestProvider> _requestProvider;

        public ProductServiceTest()
        {
            _requestProvider = new Mock<IRequestProvider>();
            _categoryService = new ProductService(_requestProvider.Object);
        }

        public Product InitModel()
        {
            return new Product { Id = 1, Name = "Product", Description = "Desc", Price = 156.25M, CategoryId = 1};
        }

        private IEnumerable<Product> InitProducts()
        {
            return new List<Product>
            {
                InitModel(),
                InitModel(),
                InitModel()
            };
        }

        [Fact]
        private async void ShouldReturnProducts()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<Product>>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitProducts());

            var results = await _categoryService.GetItemsAsync(true);

            Assert.NotEmpty(results);
        }

        [Fact]
        private async void ShouldReturnProductsByCategoryId()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<Product>>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitProducts());

            var results = await _categoryService.GetItemsAsync(new ProductCategory { Id=5}, true);

            Assert.NotEmpty(results);
        }

        [Fact]
        private async void ShouldGetProductsReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<Product>>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _categoryService.GetItemsAsync(true);

            Assert.Null(result);
        }

        [Fact]
        private async void ShouldReturnProductById()
        {
            _requestProvider.Setup(x => x.GetAsync<Product>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.GetItemAsync(1);

            Assert.NotNull(result);
            Assert.IsType<Product>(result);

            AssertProduct(result);
        }

        [Fact]
        private async void ShoulGetProductByIdReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<Product>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _categoryService.GetItemAsync(1);

            Assert.Null(result);
        }

        [Fact]
        private async void ShoulAddProduct()
        {
            _requestProvider.Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<Product>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.AddItemAsync(InitModel());


            Assert.NotNull(result);
            Assert.IsType<Product>(result);

            AssertProduct(result);
        }

        [Fact]
        private async void ShoulUpdateProduct()
        {
            _requestProvider.Setup(x => x.PutAsync(It.IsAny<string>(), It.IsAny<Product>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.UpdateItemAsync(InitModel());

            Assert.True(result);
        }

        [Fact]
        private async void ShoulDeleteProduct()
        {
            var result = await _categoryService.DeleteItemAsync(5);
            Assert.True(result);
        }

        private static void AssertProduct(Product result)
        {
            Assert.Equal(1, result.Id);
            Assert.Equal("Product", result.Name);
            Assert.Equal("Desc", result.Description);
            Assert.Equal(156.25M, result.Price);
            
        }
    }
}
