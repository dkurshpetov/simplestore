﻿using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;
using SimpleStore.Mobile.Services;
using System.Collections.Generic;
using Xunit;

namespace SimpleStore.Test.Mobile.Service
{
    public class BasketServiceTest
    {
        public IBasketService _basketService { get; set; }
        public Mock<IRequestProvider> _requestProvider;

        public BasketServiceTest()
        {
            _requestProvider = new Mock<IRequestProvider>();
            _basketService = new BasketService(_requestProvider.Object);
        }

        public BasketItem InitModel()
        {
            return new BasketItem{Id = 1, ProductId = 1, ProductName = "Product", Quantity = 3, UnitPrice = 15.6M, OldUnitPrice = 25};
        }

        private IEnumerable<BasketItem> InitList()
        {
            return new List<BasketItem>
            {
                InitModel(),
                InitModel(),
                InitModel()
            };
        }

        [Fact]
        private async void ShouldReturnBasketItems()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<BasketItem>>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitList());

            var results = await _basketService.GetItemsAsync(true);

            Assert.NotEmpty(results);
        }

        [Fact]
        private async void ShouldGetBasketItemsReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<BasketItem>>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _basketService.GetItemsAsync(true);

            Assert.Null(result);
        }

        [Fact]
        private async void ShouldReturnBasketItemById()
        {
            _requestProvider.Setup(x => x.GetAsync<BasketItem>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _basketService.GetItemAsync(1);

            Assert.NotNull(result);
            Assert.IsType<BasketItem>(result);

            AssertBasketItem(result);
        }

        [Fact]
        private async void ShouldGetBasketItemByIdReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<BasketItem>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _basketService.GetItemAsync(1);

            Assert.Null(result);
        }

        [Fact]
        private async void ShouldAddBasketItem()
        {
            _requestProvider.Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<BasketItem>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _basketService.AddItemAsync(InitModel());


            Assert.NotNull(result);
            Assert.IsType<BasketItem>(result);

            AssertBasketItem(result);
        }

        [Fact]
        private async void ShoulUpdateBaketItem()
        {
            _requestProvider.Setup(x => x.PutAsync(It.IsAny<string>(), It.IsAny<BasketItem>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _basketService.UpdateItemAsync(InitModel());

            Assert.True(result);
        }

        [Fact]
        private async void ShoulDeleteBasketItem()
        {
            var result = await _basketService.DeleteItemAsync(5);
            Assert.True(result);
        }

        private void AssertBasketItem(BasketItem basketItem)
        {
            Assert.Equal(1, basketItem.Id);
            Assert.Equal(1, basketItem.ProductId);
            Assert.Equal("Product", basketItem.ProductName);
            Assert.Equal(3, basketItem.Quantity);
            Assert.Equal(15.6M, basketItem.UnitPrice);
            Assert.Equal(46.8M, basketItem.Total);
        }
    }
}
