﻿using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;
using SimpleStore.Mobile.Services;
using System.Collections.Generic;
using Xunit;

namespace SimpleStore.Test.Mobile.Service
{
    public class OrderServiceTest
    {
        public IOrderService _orderService { get; set; }
        public Mock<IRequestProvider> _requestProvider;

        public OrderServiceTest()
        {
            _requestProvider = new Mock<IRequestProvider>();
            _orderService = new OrderService(_requestProvider.Object);
        }

        public Order InitModel()
        {
            return new Order {Id=5, Name="1587/65", Description="this is test order", Items=new List<OrderItem>() };
        }


        private IEnumerable<Order> InitOrders()
        {
            return new List<Order>
            {
                InitModel(),
                InitModel(),
                InitModel()
            };
        }

        [Fact]
        private async void ShouldReturnOrders()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<Order>>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitOrders());

            var results = await _orderService.GetItemsAsync(true);

            Assert.NotEmpty(results);
        }

        [Fact]
        private async void ShouldGetOrdersReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<Order>>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _orderService.GetItemsAsync(true);

            Assert.Null(result);
        }

        [Fact]
        private async void ShouldReturnOrderById()
        {
            _requestProvider.Setup(x => x.GetAsync<Order>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _orderService.GetItemAsync(1);

            Assert.NotNull(result);
            Assert.IsType<Order>(result);

            AssertOrder(result);
        }

        [Fact]
        private async void ShoulGetOrderByIddReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<Order>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _orderService.GetItemAsync(1);

            Assert.Null(result);
        }

        [Fact]
        private async void ShoulAddOrder()
        {
            _requestProvider.Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<Order>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _orderService.AddItemAsync(InitModel());


            Assert.NotNull(result);
            Assert.IsType<Order>(result);

            AssertOrder(result);
        }

        [Fact]
        private async void ShoulUpdateOrder()
        {
            _requestProvider.Setup(x => x.PutAsync(It.IsAny<string>(), It.IsAny<Order>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _orderService.UpdateItemAsync(InitModel());

            Assert.True(result);
        }


        [Fact]
        private async void ShoulDeleteOrder()
        {
            var result = await _orderService.DeleteItemAsync(5);
            Assert.True(result);
        }

        private static void AssertOrder(Order order)
        {
            Assert.Equal(5, order.Id);
            Assert.Equal("1587/65", order.Name);
            Assert.Equal("this is test order", order.Description);
            Assert.Empty(order.Items);
        }
    }

}
