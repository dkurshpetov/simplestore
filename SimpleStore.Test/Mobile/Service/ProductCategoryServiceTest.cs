﻿using Moq;
using SimpleStore.Mobile.Models;
using SimpleStore.Mobile.RequestProvider;
using SimpleStore.Mobile.Services;
using System.Collections.Generic;
using Xunit;

namespace SimpleStore.Test.Mobile.Service
{
    public class ProductCategoryServiceTest 
    {
        public IDataStore<ProductCategory> _categoryService { get; set; }
        public Mock<IRequestProvider> _requestProvider;

        public ProductCategoryServiceTest()
        {
            _requestProvider = new Mock<IRequestProvider>();
            _categoryService = new ProductCategoryService(_requestProvider.Object);
        }
        
        public ProductCategory InitModel()
        {
            return new ProductCategory{Id = 1, Name = "Category", Description = "Desc"};
        }

        private IEnumerable<ProductCategory> InitListCategories()
        {
            return  new List<ProductCategory>
            {
                InitModel(),
                InitModel(),
                InitModel()
            };
        }

        [Fact]
        private async void ShouldReturnCategories()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<ProductCategory>>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitListCategories());

            var results = await _categoryService.GetItemsAsync(true);

            Assert.NotEmpty(results);
        }

        [Fact]
        private async void ShouldGetCategoriesReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<IEnumerable<ProductCategory>>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _categoryService.GetItemsAsync(true);

            Assert.Null(result);
        }

        [Fact]
        private async void ShouldReturnCategoryById()
        {
            _requestProvider.Setup(x => x.GetAsync<ProductCategory>(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.GetItemAsync(1);

            Assert.NotNull(result);
            Assert.IsType<ProductCategory>(result);

            AssertCategory(result);
        }
        
        [Fact]
        private async void ShouldGetCategoryReturnNullWhenRequestProviderThrowException()
        {
            _requestProvider.Setup(x => x.GetAsync<ProductCategory>(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<HttpRequestExceptionEx>();

            var result = await _categoryService.GetItemAsync(1);

            Assert.Null(result);
        }

        [Fact]
        private async void ShoulAddCategory()
        {
            _requestProvider.Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<ProductCategory>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.AddItemAsync(InitModel());

            Assert.NotNull(result);
            Assert.IsType<ProductCategory>(result);

            AssertCategory(result);
        }

        [Fact]
        private async void ShoulUpdateCategory()
        {
            _requestProvider.Setup(x => x.PutAsync(It.IsAny<string>(), It.IsAny<ProductCategory>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(InitModel());

            var result = await _categoryService.UpdateItemAsync(InitModel());

            Assert.True(result);
        }

        [Fact]
        private async void ShoulDeleteCategory()
        {
            var result = await _categoryService.DeleteItemAsync(5);
            Assert.True(result);
        }

        private static void AssertCategory(ProductCategory result)
        {
            Assert.Equal(1, result.Id);
            Assert.Equal("Category", result.Name);
            Assert.Equal("Desc", result.Description);
        }
    }
}
