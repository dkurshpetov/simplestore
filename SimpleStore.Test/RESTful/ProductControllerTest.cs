﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SimpleStore.BackEnd.DAL.Interfaces;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.BackEnd.RESTful.Controllers;
using Xunit;

namespace SimpleStore.Test.RESTful
{
    public class ProductControllerTest : ApiControllerGenericTest<Product, IProductRepository>
    {
        public ProductControllerTest()
        {
            Controller = new ProductController(Repository.Object, Logger.Object);;
        }

        [Fact]
        public void ShouldReturnProductByCategoryId()
        {
            Repository.Setup(x => x.GetByCategoryId(It.IsAny<int>())).ReturnsAsync(InitModelList());

            var response = ((ProductController)Controller).GetByCategoryId(1);

            AssertRequestResult<OkObjectResult>(response);

            var result = (OkObjectResult)response.Result;
            var list = (IEnumerable<Product>)(result.Value);

            Assert.NotEmpty(list);
        }

        [Fact]
        public void ShouldReturnConflictResponseWhenRepositoryGetByCategoryIddRerurnThrowsException()
        {
            Repository.Setup(x => x.GetByCategoryId(It.IsAny<int>()))
                .Throws<Exception>();

            var response = ((ProductController)Controller).GetByCategoryId(1);

            AssertRequestResult<ConflictResult>(response);
        }


        public override Product InitModel()
        {
            return new Product
            {
                Id = 1,
                Name = "Product 1",
                Description =  "Description",
                Price =15.6M,
                Category = new ProductCategory
                {
                    Id = 6,
                    Name = "Category 1",
                    Description = "Descr"
                }

            };
        }

        public override List<Product> InitModelList()
        {
            return new List<Product>
            {
                new Product(),
                new Product(),
                new Product(),
            };
        }

        public override void AssertModel(Product product)
        {
            Assert.Equal(1, product.Id);
            Assert.Equal("Product 1", product.Name);
            Assert.Equal("Description", product.Description);
            Assert.Equal(15.6M, product.Price);
            Assert.Null(product.Discount);

            Assert.Equal(6, product.Category.Id);
            Assert.Equal("Category 1", product.Category.Name);
            Assert.Equal("Descr", product.Category.Description);
        }
    }

}
