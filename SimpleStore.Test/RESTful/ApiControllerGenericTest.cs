﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using SimpleStore.BackEnd.RESTful.Controllers;
using SimpleStore.DAL.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace SimpleStore.Test.RESTful
{
    public abstract class ApiControllerGenericTest<T, TRepository> where T : class where TRepository : class, IRepository<T>
    {
        public ApiControllerGeneric<T> Controller;
        public Mock<TRepository> Repository;
        public Mock<ILogger<T>> Logger;

        public ApiControllerGenericTest()
        {
            Repository = new Mock<TRepository>();
            Logger = new Mock<ILogger<T>>();
        }

        public abstract T InitModel();

        public abstract List<T> InitModelList();

        public abstract void AssertModel(T model);

        [Fact]
        public void ShouldReturnModels()
        {
            Repository.Setup(x => x.GetAll()).ReturnsAsync(InitModelList());

            var response = Controller.Get();

            AssertRequestResult<OkObjectResult>(response);
            var result = (OkObjectResult)response.Result;

            Assert.NotEmpty((IEnumerable)result.Value);
        }

        [Fact]
        public void ShouldReturnConflictResponseWhenRepositoryGetAllRerurnThrowsException()
        {
            Repository.Setup(x => x.GetAll())
                .Throws<Exception>();

            var response = Controller.Get();

            AssertRequestResult<ConflictResult>(response);
        }

        [Fact]
        public void ShouldReturnModelById()
        {
            Repository.Setup(x => x.Get(It.IsAny<int>())).ReturnsAsync(InitModel);

            var response = Controller.Get(1);

            AssertRequestResult<OkObjectResult>(response);

            var result = (OkObjectResult)response.Result;
            var model = (T)(result.Value);

            AssertModel(model);
        }

        [Fact]
        public void ShouldReturnConflictResponseWhenRepositoryGetByIdRerurnThrowsException()
        {
            Repository.Setup(x => x.Get(It.IsAny<int>()))
                .Throws<Exception>();

            var response = Controller.Get(5);

            AssertRequestResult<ConflictResult>(response);
        }

        [Fact]
        public void ShouldReturnNonContentResponseWhenRepositoryGetByIdRerurnNull()
        {
            Repository.Setup(x => x.Get(It.IsAny<int>()))
                .ReturnsAsync((T)null);

            var response = Controller.Get(5);

            AssertRequestResult<NoContentResult>(response);
        }

        [Fact]
        public void ShouldPostReturOkRequst()
        {
            var response = Controller.Post(InitModel());

            AssertRequestResult<OkObjectResult>(response);

            Repository.Verify(x => x.Add(It.IsAny<T>()));
            Repository.Verify(x => x.Save());
        }

        [Fact]
        public void ShouldPostReturnBadRequstResponseWhenModelEquelNull()
        {
            var response = Controller.Post(null);

            AssertRequestResult<BadRequestObjectResult>(response);

            Repository.Verify(x => x.Add(It.IsAny<T>()), Times.Never);
            Repository.Verify(x => x.Save(), Times.Never);
        }

        [Fact]
        public void ShouldPostReturnConflictResponseWhenDontUpdate()
        {
            Repository.Setup(x => x.Add(It.IsAny<T>())).
                Throws<Exception>();

            var response = Controller.Post(InitModel());

            AssertRequestResult<ConflictObjectResult>(response);

            Repository.Verify(x => x.Add(It.IsAny<T>()), Times.Once);

            Repository.Verify(x => x.Save(), Times.Never);
        }

        [Fact]
        public void ShouldPostReturnConflictResponseWhenDontSave()
        {
            Repository.Setup(x => x.Save()).
                Throws<Exception>();

            var response = Controller.Post(InitModel());

            AssertRequestResult<ConflictObjectResult>(response);

            Repository.Verify(x => x.Add(It.IsAny<T>()), Times.Once);
            Repository.Verify(x => x.Save(), Times.Once);
        }

        [Fact]
        public void ShouldPutReturOkRequst()
        {
            var response = Controller.Put(1, InitModel());

            AssertRequestResult<OkObjectResult>(response);

            Repository.Verify(x => x.Update(It.IsAny<T>()));
            Repository.Verify(x => x.Save());
        }

        [Fact]
        public void ShouldPutReturnConflictResponseWhenDontUpdate()
        {
            Repository.Setup(x => x.Update(It.IsAny<T>())).
                Throws<Exception>();

            var response = Controller.Put(1, InitModel());

            AssertRequestResult<ConflictObjectResult>(response);

            Repository.Verify(x => x.Update(It.IsAny<T>()), Times.Once);
        }

        [Fact]
        public void ShouldPutReturnConflictResponseWhenDontSave()
        {
            Repository.Setup(x => x.Save()).
                Throws<Exception>();

            var response = Controller.Put(1, InitModel());

            AssertRequestResult<ConflictObjectResult>(response);

            Repository.Verify(x => x.Update(It.IsAny<T>()), Times.Once);
            Repository.Verify(x => x.Save(), Times.Once);
        }

        [Fact]
        public void ShouldPutReturnBadRequstResponseWhenModelEquelNull()
        {
            var response = Controller.Put(1, null);
            AssertRequestResult<BadRequestObjectResult>(response);
        }

        [Fact]
        public void ShouldDeleteReturnBadRequstResponseWhenDontFind()
        {
            Repository.Setup(x => x.Get(It.IsAny<int>()))
                .ReturnsAsync((T)null);

            var response = Controller.Delete(1);

            AssertRequestResult<BadRequestObjectResult>(response);

            Repository.Verify(x => x.Update(It.IsAny<T>()), Times.Never);
            Repository.Verify(x => x.Save(), Times.Never);
        }

        [Fact]
        public void ShouldDeleteModel()
        {
            Repository.Setup(x => x.Get(It.IsAny<int>()))
                .ReturnsAsync(InitModel);

            var response = Controller.Delete(1);

            AssertRequestResult<OkResult>(response);

            Repository.Verify(x => x.Remove(It.IsAny<T>()), Times.Once);
            Repository.Verify(x => x.Save(), Times.Once);
        }

        public void AssertRequestResult<TResult>(Task<ActionResult> response)
        {
            Assert.NotNull(response);
            Assert.NotNull(response.Result);
            Assert.IsType<TResult>(response.Result);
        }
    }
}
