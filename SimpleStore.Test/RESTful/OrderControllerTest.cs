﻿using System.Collections.Generic;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.BackEnd.RESTful.Controllers;
using SimpleStore.DAL.Interfaces;

namespace SimpleStore.Test.RESTful
{
    public class OrderControllerTest : ApiControllerGenericTest<Order, IOrderRepository>
    {

        public OrderControllerTest()
        {
            Controller = new OrderController(Repository.Object, Logger.Object);
        }

        public override void AssertModel(Order model)
        {

        }

        public override Order InitModel()
        {
            return new Order {Name = "test-order", IsCheck = false, Items = new List<OrderItem> {new OrderItem()}};
        }

        public override List<Order> InitModelList()
        {
            return new List<Order> {new Order()};
        }
    }
}