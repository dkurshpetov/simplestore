﻿using System.Collections.Generic;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.BackEnd.RESTful.Controllers;
using SimpleStore.DAL.Interfaces;
using Xunit;

namespace SimpleStore.Test.RESTful
{
    public class ProductCategoryControllerTest : ApiControllerGenericTest<ProductCategory, IProductCategoryRepository>
    {
        public ProductCategoryControllerTest()
        {
            Controller = new ProductCategoryController(Repository.Object, Logger.Object);
        }

        public override ProductCategory InitModel()
        {
            return new ProductCategory
            {
                Id = 1,
                Name = "Category",
                Description = "Description",
                Products = new List<Product>
                {
                    new Product(),
                    new Product(),
                    new Product(),
                }
                
            };
        }

        public override List<ProductCategory> InitModelList()
        {
            return new List<ProductCategory>
            {
                InitModel(),
                InitModel(),
                InitModel()
            };
        }

        public override void AssertModel(ProductCategory productCategory)
        {
            Assert.Equal(1, productCategory.Id);
            Assert.Equal("Category", productCategory.Name);
            Assert.Equal("Description", productCategory.Description);
            Assert.NotEmpty(productCategory.Products);
        }
    }
}
