﻿using SimpleStore.BackEnd.Domain.Entities;

namespace SimpleStore.DAL.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}