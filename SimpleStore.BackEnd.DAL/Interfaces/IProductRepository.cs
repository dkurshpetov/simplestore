﻿using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleStore.BackEnd.DAL.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IEnumerable<Product>> GetByCategoryId(int categoryid);
    }
}
