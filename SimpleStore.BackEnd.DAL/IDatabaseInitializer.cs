﻿using System.Threading.Tasks;

namespace SimpleStore.BackEnd.DAL
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}
