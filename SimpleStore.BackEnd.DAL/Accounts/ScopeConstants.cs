﻿
namespace SimpleStore.BackEnd.DAL.Accounts
{
    public static class ScopeConstants
    {
        ///<summary>A scope that specifies the roles of an entity</summary>
        public const string Roles = "roles";
    }
}
