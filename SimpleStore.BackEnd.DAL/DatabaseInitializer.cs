﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using SimpleStore.DAL.Repositories;
using SimpleStore.BackEnd.DAL.Accounts;
using SimpleStore.BackEnd.Domain.Entities;
using System.Collections.Generic;

namespace SimpleStore.BackEnd.DAL
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager, ILogger<DatabaseInitializer> logger)
        {
            _accountManager = accountManager;
            _context = context;
            _logger = logger;
        }

        public async Task SeedAsync()
        {

            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Users.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt accounts");

                const string adminRoleName = "administrator";
                const string userRoleName = "user";

                await EnsureRoleAsync(adminRoleName, "Default administrator", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync(userRoleName, "Default user", new string[] { });

                await CreateUserAsync("admin", "tempP@ss123", "Inbuilt Administrator", "admin@ebenmonney.com", "+1 (123) 000-0000", new string[] { adminRoleName });
                await CreateUserAsync("user", "tempP@ss123", "Inbuilt Standard User", "user@ebenmonney.com", "+1 (123) 000-0001", new string[] { userRoleName });

                _logger.LogInformation("Inbuilt account generation completed");
            }


            if (!await _context.ProductCategories.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt catigories");

                var catigories = new List<ProductCategory>
                {
                    new ProductCategory {  Name = "First category", Description="This is an item description.", Products = new List<Product>{
                        new Product{
                        Name = "Product 1",
                        Description =  "Description Product 1",
                        Price =15.6M

                        },

                        new Product{
                        Name = "Product 2",
                        Description =  "Description Product 2",
                        Price =15.6M,
                        Discount = 2M,

                    },

                        new Product{
                            Name = "Product 3",
                            Description =  "Description Product 3",
                            Price =25.6M

                        },
                    }

                    },

                    new ProductCategory {  Name = "Second category", Description="This is an item description.", Products = new List<Product>{
                            new Product{
                                Name = "Product 4",
                                Description =  "Description Product 4",
                                Price =15.6M

                            },

                            new Product{
                                Name = "Product 5",
                                Description =  "Description Product 5",
                                Price =15.6M,
                                Discount = 3M,

                            },

                        }

                    },
                    new ProductCategory {  Name = "Third category", Description="This is an item description." },

                };


                await _context.ProductCategories.AddRangeAsync(catigories);

                _logger.LogInformation("Inbuilt categoris generation completed");
            }

            await _context.SaveChangesAsync();
        }


        private async Task EnsureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _accountManager.GetRoleByNameAsync(roleName)) == null)
            {
                ApplicationRole applicationRole = new ApplicationRole(roleName, description);

                var result = await this._accountManager.CreateRoleAsync(applicationRole, claims);

                if (!result.Succeeded)
                    throw new Exception($"Seeding \"{description}\" role failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string fullName, string email, string phoneNumber, string[] roles)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName,
                FullName = fullName,
                Email = email,
                PhoneNumber = phoneNumber,
                EmailConfirmed = true,
                IsEnabled = true
            };

            var result = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!result.Succeeded)
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");


            return applicationUser;
        }
    }
}
