﻿using Microsoft.EntityFrameworkCore;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Interfaces;
using SimpleStore.DAL.Repositories;
using System.Threading.Tasks;

namespace SimpleStore.BackEnd.DAL.Repositories
{
    public class ProductCategoryRepository : Repository<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<ProductCategory> Get(int id)
        {
            return await Context.ProductCategories
                .Include(s => s.Products)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
