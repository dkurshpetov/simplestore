﻿using Microsoft.EntityFrameworkCore;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Interfaces;
using System.Threading.Tasks;

namespace SimpleStore.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
        }

        public override async Task<Order> Get(int id)
        {
            return await Context.Orders
                .Include(o => o.Items)
                .ThenInclude(i => i.Product)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}