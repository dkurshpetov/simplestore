﻿using Microsoft.EntityFrameworkCore;
using SimpleStore.BackEnd.DAL.Interfaces;
using SimpleStore.BackEnd.Domain.Entities;
using SimpleStore.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleStore.BackEnd.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Product>> GetByCategoryId(int categoryid)
        {
            return await Context.Products
                         .Include(x=>x.Category)
                         .Where(x => x.Category.Id == categoryid).ToListAsync();
        }
    }
}
